# Window Title Logger #
*Mainly intended for use as an [osu!](https://osu.ppy.sh/) now playing tool for livestreamers*

### What? ###

This application watches a process you choose for changes to the window title, and saves that title to a .txt file.

### Why? ###
Add the log file to your livestream layout, and it serves as a nice "now playing" indicator for osu! streams. Can be used for other purposes too, of course. (For example, a Spotify now playing log)

### How? ###
Mainly using [SetWinEventHook](http://msdn.microsoft.com/en-us/library/windows/desktop/dd373640%28v=vs.85%29.aspx) to watch a process for a name change event, and some basic C# functions in [System.Diagnostics.Process](http://msdn.microsoft.com/en-us/library/system.diagnostics.process%28v=vs.110%29.aspx) to grab the window title.

### Usage ###
* Download the latest release (download coming soon) or compile your own, and put it somewhere you like
* Run WindowTitleLogger.exe
* If you'd like to change the log file, click the ... button and choose a file. The default setting will create the file in the same directory as the .exe.
* Select a process from the list (refresh if what you're looking for isn't there) and click start

The window title should now be stored in the output file, and update on every change until you click stop or close either the logger app or the monitored app.

### Misc ###
If you'd like to see my plans for this application you can view the mindmap here:
[MindMap on Mind42](http://mind42.com/mindmap/e6f7fab7-176a-4ede-b379-93d817a1331f)

### Notes for editing/compiling yourself ###
* There is a chance you'll get a console window coming up when running the debug version. Reasoning for this should be pretty self explanatory. To get rid of it, head into the project properties and change *Output Type* from *Console Application* to *Windows Application* (This is the process for Visual Studio; if you're using a different editor, it will likely be different)