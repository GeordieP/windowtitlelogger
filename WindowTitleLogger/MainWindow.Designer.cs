﻿namespace WindowTitleLogger {
    partial class MainWindow {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.label1 = new System.Windows.Forms.Label();
            this.procListComboBox = new System.Windows.Forms.ComboBox();
            this.startMonitor_Btn = new System.Windows.Forms.Button();
            this.stopMonitor_Btn = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.chooseLogFilePath_Btn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.logFilePath_TextBox = new System.Windows.Forms.TextBox();
            this.refreshProcList_Btn = new System.Windows.Forms.Button();
            this.statusInfo_txtBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.excludeStringTextBox = new System.Windows.Forms.TextBox();
            this.excludeBtn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Select window to monitor";
            // 
            // procListComboBox
            // 
            this.procListComboBox.CausesValidation = false;
            this.procListComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.procListComboBox.FormattingEnabled = true;
            this.procListComboBox.Location = new System.Drawing.Point(13, 26);
            this.procListComboBox.Name = "procListComboBox";
            this.procListComboBox.Size = new System.Drawing.Size(446, 21);
            this.procListComboBox.TabIndex = 1;
            // 
            // startMonitor_Btn
            // 
            this.startMonitor_Btn.Location = new System.Drawing.Point(12, 92);
            this.startMonitor_Btn.Name = "startMonitor_Btn";
            this.startMonitor_Btn.Size = new System.Drawing.Size(130, 37);
            this.startMonitor_Btn.TabIndex = 2;
            this.startMonitor_Btn.Text = "Start";
            this.startMonitor_Btn.UseVisualStyleBackColor = true;
            this.startMonitor_Btn.Click += new System.EventHandler(this.startMonitorButton_Click);
            // 
            // stopMonitor_Btn
            // 
            this.stopMonitor_Btn.Location = new System.Drawing.Point(149, 92);
            this.stopMonitor_Btn.Name = "stopMonitor_Btn";
            this.stopMonitor_Btn.Size = new System.Drawing.Size(130, 37);
            this.stopMonitor_Btn.TabIndex = 3;
            this.stopMonitor_Btn.Text = "Stop";
            this.stopMonitor_Btn.UseVisualStyleBackColor = true;
            this.stopMonitor_Btn.Click += new System.EventHandler(this.stopMonitorButton_Click);
            // 
            // chooseLogFilePath_Btn
            // 
            this.chooseLogFilePath_Btn.Location = new System.Drawing.Point(465, 63);
            this.chooseLogFilePath_Btn.Name = "chooseLogFilePath_Btn";
            this.chooseLogFilePath_Btn.Size = new System.Drawing.Size(68, 23);
            this.chooseLogFilePath_Btn.TabIndex = 4;
            this.chooseLogFilePath_Btn.Text = "...";
            this.chooseLogFilePath_Btn.UseVisualStyleBackColor = true;
            this.chooseLogFilePath_Btn.Click += new System.EventHandler(this.chooseLogFilePath_Btn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Log file path";
            // 
            // logFilePath_TextBox
            // 
            this.logFilePath_TextBox.Location = new System.Drawing.Point(13, 66);
            this.logFilePath_TextBox.Name = "logFilePath_TextBox";
            this.logFilePath_TextBox.Size = new System.Drawing.Size(446, 20);
            this.logFilePath_TextBox.TabIndex = 6;
            // 
            // refreshProcList_Btn
            // 
            this.refreshProcList_Btn.Location = new System.Drawing.Point(465, 26);
            this.refreshProcList_Btn.Name = "refreshProcList_Btn";
            this.refreshProcList_Btn.Size = new System.Drawing.Size(68, 21);
            this.refreshProcList_Btn.TabIndex = 7;
            this.refreshProcList_Btn.Text = "Refresh";
            this.refreshProcList_Btn.UseVisualStyleBackColor = true;
            this.refreshProcList_Btn.Click += new System.EventHandler(this.refreshProcList_Btn_Click);
            // 
            // statusInfo_txtBox
            // 
            this.statusInfo_txtBox.AcceptsReturn = true;
            this.statusInfo_txtBox.Enabled = false;
            this.statusInfo_txtBox.Location = new System.Drawing.Point(12, 148);
            this.statusInfo_txtBox.Multiline = true;
            this.statusInfo_txtBox.Name = "statusInfo_txtBox";
            this.statusInfo_txtBox.Size = new System.Drawing.Size(521, 59);
            this.statusInfo_txtBox.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Status Information";
            // 
            // excludeStringTextBox
            // 
            this.excludeStringTextBox.Location = new System.Drawing.Point(12, 236);
            this.excludeStringTextBox.Name = "excludeStringTextBox";
            this.excludeStringTextBox.Size = new System.Drawing.Size(447, 20);
            this.excludeStringTextBox.TabIndex = 10;
            // 
            // excludeBtn
            // 
            this.excludeBtn.Location = new System.Drawing.Point(465, 236);
            this.excludeBtn.Name = "excludeBtn";
            this.excludeBtn.Size = new System.Drawing.Size(68, 20);
            this.excludeBtn.TabIndex = 11;
            this.excludeBtn.Text = "Exclude";
            this.excludeBtn.UseVisualStyleBackColor = true;
            this.excludeBtn.Click += new System.EventHandler(this.excludeBtn_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 214);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(196, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Exclude Strings (Seperate with commas)";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(545, 268);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.excludeBtn);
            this.Controls.Add(this.excludeStringTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.statusInfo_txtBox);
            this.Controls.Add(this.refreshProcList_Btn);
            this.Controls.Add(this.logFilePath_TextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.chooseLogFilePath_Btn);
            this.Controls.Add(this.stopMonitor_Btn);
            this.Controls.Add(this.startMonitor_Btn);
            this.Controls.Add(this.procListComboBox);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainWindow";
            this.Text = "WindowTitleLogger";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox procListComboBox;
        private System.Windows.Forms.Button startMonitor_Btn;
        private System.Windows.Forms.Button stopMonitor_Btn;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button chooseLogFilePath_Btn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox logFilePath_TextBox;
        private System.Windows.Forms.Button refreshProcList_Btn;
        private System.Windows.Forms.TextBox statusInfo_txtBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox excludeStringTextBox;
        private System.Windows.Forms.Button excludeBtn;
        private System.Windows.Forms.Label label4;
    }
}

