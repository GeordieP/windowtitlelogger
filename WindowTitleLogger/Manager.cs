﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowTitleLogger {
    public sealed class Manager {
        #region Singleton management;
        private static readonly Manager instance = new Manager();
        private Manager() { }
        public static Manager Instance {
            get { return instance; }
        }
        #endregion;

        private TextBox statusTextBox;
        public TextBox StatusTextBox {
            set { statusTextBox = value; }
        }

        private Dictionary<string, string> statusInfoLines;

        public void Init() {
            statusInfoLines = new Dictionary<string, string>();
            statusInfoLines.Add("MonitorStatus", "Stopped");
            statusInfoLines.Add("MonitoredApplication", "-");
            statusInfoLines.Add("CurrentWindowTitle", "-");
            statusInfoLines.Add("LastLog", "-");
            PushStatusUpdate();
        }

        public void UpdateStatus(string _key, string _val) {
            statusInfoLines[_key] = _val;
            PushStatusUpdate();
        }

        private void PushStatusUpdate() {
            string outString;
            outString = String.Format(
                "Monitor Status: {0}\r\nMonitored Application: {1}\r\nCurrent Window Title: {2}\r\nLast Text Written: {3}",
                statusInfoLines["MonitorStatus"],
                statusInfoLines["MonitoredApplication"],
                statusInfoLines["CurrentWindowTitle"],
                statusInfoLines["LastLog"]
            );

            statusTextBox.Text = outString;
        }
    }
}
