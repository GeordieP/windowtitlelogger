﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace WindowTitleLogger {
    public class WindowTitleLogger {
        public MainWindow mainWindow;
        ProcessMonitor procMon = ProcessMonitor.Instance;   // get instance of the process monitor singleton
        Dictionary<int, string> processList;                // store a dictionary of process IDs (int) and their respective window information (process name & window title)

        public WindowTitleLogger() { }

        public void Initialize() {
            procMon.RefreshProcessArray();
        }

        public Dictionary<int, string> GetProcessList() {
            // clear the list every time
            processList = new Dictionary<int, string>();
            procMon.RefreshProcessArray();

            foreach (Process p in procMon.ProcessArray) {
                if (!String.IsNullOrEmpty(p.MainWindowTitle)) {
                    processList.Add(p.Id, String.Format("[{0}] {1}", p.ProcessName, p.MainWindowTitle));
                }
            }
            return processList;
        }

        public void ChangeMonitoredProcess(int selectedIndex) {
            Manager.Instance.UpdateStatus("MonitoredApplication", processList.ElementAt(selectedIndex).Value);
            Manager.Instance.UpdateStatus("MonitorStatus", "Active");

            procMon.WatchProcess(processList.ElementAt(selectedIndex).Key);
        }

        public void ReleaseProcess() {
            Manager.Instance.UpdateStatus("MonitorStatus", "Stopped");
            Manager.Instance.UpdateStatus("MonitoredApplication", "-");

            procMon.ReleaseProcess();
        }
    }
}
