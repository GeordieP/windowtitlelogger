﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Runtime.InteropServices;


namespace WindowTitleLogger {
    public sealed class ProcessMonitor {
    #region Singleton management;
        private static readonly ProcessMonitor instance = new ProcessMonitor();
        private ProcessMonitor() { }        
        public static ProcessMonitor Instance {
            get { return instance; }
        }
    #endregion;

    #region TitleEventHandler;
        delegate void WinEventDelegate(IntPtr hWinEventHook, uint eventType, IntPtr hwnd, int idObject, int idChild, uint dwEventThread, uint dwmsEventTime, int processID);
        static WinEventDelegate procDelegate = new WinEventDelegate(WinEventProc);

        [DllImport("user32.dll")]
        static extern IntPtr SetWinEventHook(uint eventMin, uint eventMax, IntPtr hmodWinEventProc, WinEventDelegate lpfnWinEventProc, int idProcess, uint idThread, uint dwFlags);
        [DllImport("user32.dll")]
        static extern bool UnhookWinEvent(IntPtr hWinEventHook);

        const uint EVENT_OBJECT_NAMECHANGE = 0x800C;
        const uint WINEVENT_OUTOFCONTEXT = 0;
    #endregion;

        private Process[] processArray;
        public Process[] ProcessArray {
            get { return processArray; }
            set { processArray = value; }
        }

        static private int monitoredProcessID = 0;
        static private IntPtr hHook = IntPtr.Zero;

        public void RefreshProcessArray() {
            processArray = Process.GetProcesses();
        }

        public void WatchProcess(int _procID) {
            // TODO: Invalid procID checking
            ReleaseProcess();       // make sure to unhook any active windows so we don't get duplicate ouputs
            monitoredProcessID = _procID;
            hHook = SetWinEventHook(EVENT_OBJECT_NAMECHANGE, EVENT_OBJECT_NAMECHANGE, IntPtr.Zero, procDelegate, monitoredProcessID, 0, WINEVENT_OUTOFCONTEXT);

            // log the initial window title
            string temp = GetWindowName(monitoredProcessID);
            Manager.Instance.UpdateStatus("CurrentWindowTitle", temp);
            Manager.Instance.UpdateStatus("LastLog", temp);
            FileManager.Instance.LogToFile(temp);

#if DEBUG
            Console.WriteLine("Began monitoring process {0}", _procID);
#endif
        }

        public void ReleaseProcess() {
            UnhookWinEvent(hHook);
            Manager.Instance.UpdateStatus("CurrentWindowTitle", "-");

#if DEBUG
            Console.WriteLine("Stopped monitoring process {0}", monitoredProcessID);
#endif
        }

        static void WinEventProc(IntPtr hWinEventHook, uint eventType, IntPtr hwnd, int idObject, int idChild, uint dwEventThread, uint dwmsEventTime, int processID) {
            if (idObject != 0 || idChild != 0) return;

            string temp = GetWindowName(monitoredProcessID);

            Manager.Instance.UpdateStatus("CurrentWindowTitle", temp);
            FileManager.Instance.LogToFile(temp);
#if DEBUG
            Console.WriteLine("ProcID: " + monitoredProcessID);
            Console.WriteLine("Text of hwnd changed {0:x8}", hwnd.ToInt32());
            Console.WriteLine("New Title: {0}", GetWindowName(monitoredProcessID));
#endif
        }

        static string GetWindowName(int procID) {
            return Process.GetProcessById(procID).MainWindowTitle;
            return "unknown";
        }
    }
}
