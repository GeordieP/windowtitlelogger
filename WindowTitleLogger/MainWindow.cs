﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowTitleLogger {
    public partial class MainWindow : Form {
        public WindowTitleLogger windowTitleLogger;

        public MainWindow() {
            InitializeComponent();

            // give the manager access to the status text box
            Manager.Instance.StatusTextBox = statusInfo_txtBox;

            excludeStringTextBox.Text = FileManager.Instance.LoadExcludeStrings();
            ParseExcludeString();
        }

        public void UpdateStatusBox(string _text) {
            statusInfo_txtBox.Text = _text;
        }

        private void Form1_Load(object sender, EventArgs e) {
            RefreshProcList();

            // set up save file dialog box with the default filepath
            saveFileDialog1.FileName = FileManager.Instance.FilePath;

            // set up output file path defaults
            logFilePath_TextBox.Text = FileManager.Instance.FilePath;
        }

        private void startMonitorButton_Click(object sender, EventArgs e) {
            windowTitleLogger.ChangeMonitoredProcess(procListComboBox.SelectedIndex);
        }

        private void stopMonitorButton_Click(object sender, EventArgs e) {
            windowTitleLogger.ReleaseProcess();
        }

        private void chooseLogFilePath_Btn_Click(object sender, EventArgs e) {
            saveFileDialog1.ShowDialog();

            string path = saveFileDialog1.FileName;
            if (!String.IsNullOrEmpty(path)) {
                FileManager.Instance.ChangeFilePath(path);
                logFilePath_TextBox.Text = FileManager.Instance.FilePath;
            } else {
                MessageBox.Show("Invalid file path, please choose a valid location", "Invalid path", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void refreshProcList_Btn_Click(object sender, EventArgs e) {
            RefreshProcList();
        }

        private void RefreshProcList() {
            procListComboBox.Items.Clear();
            Dictionary<int, string> processList = windowTitleLogger.GetProcessList();
            procListComboBox.Items.AddRange(processList.Values.ToArray<string>());
            procListComboBox.SelectedIndex = 0;
        }

        private void excludeBtn_Click(object sender, EventArgs e) {
            ParseExcludeString();
        }

        private void ParseExcludeString() {
            List<string> temp = new List<string>();
            string[] temparray = excludeStringTextBox.Text.Split(',');
            temp = temparray.ToList<string>();
            FileManager.Instance.SetExcludeStrings(temp);
            FileManager.Instance.SaveExcludeStrings(excludeStringTextBox.Text);
        }
    }
}
