﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowTitleLogger {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            WindowTitleLogger wtl = new WindowTitleLogger();
            MainWindow mainWindow = new MainWindow();
            Manager.Instance.Init();

            mainWindow.windowTitleLogger = wtl;
            wtl.mainWindow = mainWindow;
            wtl.Initialize();       // starts the whole process

            
            Application.Run(mainWindow);
        }
    }
}
