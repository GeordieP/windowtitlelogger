﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowTitleLogger {
    public sealed class FileManager {
    #region Singleton management;
        private static readonly FileManager instance = new FileManager();
        private FileManager() { }        
        public static FileManager Instance {
            get { return instance; }
        }
    #endregion;

        string excludeStringsFilePath = "ExcludeStrings.txt";

        private string filePath =  "WindowTitleLogFile.txt";
        public string FilePath {
            get { return filePath; }
        }

        private List<string> excludeStrings = new List<string>();


        public void ChangeFilePath(string _filePath) {
            filePath = _filePath;
#if DEBUG
            Console.WriteLine("set file path to {0}", filePath);
#endif
        }

        public void LogToFile(string _text) {
            if (excludeStrings.Count > 0) {
                for (int i = 0; i < excludeStrings.Count; i++) {
                    if (!String.IsNullOrWhiteSpace(excludeStrings[i]) && _text.Contains(excludeStrings[i])) {
                        _text = _text.Replace(excludeStrings[i], "");
                    }
                }
            }
                Manager.Instance.UpdateStatus("LastLog", _text);
                System.IO.File.WriteAllText(filePath, _text, Encoding.UTF8);
        }

        public void SetExcludeStrings(List<string> _exclStrs) {
            excludeStrings = _exclStrs;
        }

        public void SaveExcludeStrings(string _excludeString) {
            System.IO.File.WriteAllText(excludeStringsFilePath, _excludeString, Encoding.UTF8);
        }

        public string LoadExcludeStrings() {
            if (System.IO.File.Exists(excludeStringsFilePath)) 
                return System.IO.File.ReadAllText(excludeStringsFilePath, Encoding.UTF8);
            return "";
        }
    }
}
